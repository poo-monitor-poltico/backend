
# backend

Este repositório armazena todo o código frontend do projeto Monitor Politico. Para acessar os documentos entre no nosso repositório de documentação: https://gitlab.com/poo-monitor-poltico/documentation.

## Contexto

Tópicos Avançados em Programação Orientada a Objetos

Esse projeto foi desenvolvido no contexto da disciplina **MAC413/5714 - Tópicos Avançados de Programação Orientada a Objetos** que visa discutir conceitos avançados de modelagem e desenvolvimento do software orientado a objetos. Deste modo, é trabalhado os aspectos de desenvolvimento buscando identificar padrões arquiteturais que melhor atendam as necessidades dos requisitos. Isso inclui atividades de concepção, especificação e de desenvolvimento de um software, que vai desde a interface do usuário até os algoritmos e estruturas de dados necessários para implementar o software.

Dentre as opções o tema escolhido foi o **Monitor Politico**, que visa monitorar partidos e deputados em relação à proposições, votações e transparência das informações da forma mais simples e visualmente agradavel.

### Documentação do Projeto
- [Requisitos do projeto](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/requisitos/requisitos.md)
- [Documento de Arquitetura](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/arquitetura/docArquitetura.md)

## Tecnologias utilizadas
- [Spring](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.5.4&packaging=jar&jvmVersion=11&groupId=br.ime.usp.MAC0413&artifactId=poo-monitor-poltico&name=poo-monitor-poltico&description=Demo%20project%20for%20Spring%20Boot&packageName=br.ime.usp.MAC0413.poo-monitor-poltico&dependencies=devtools)
- [Docker](https://www.docker.com/) [Versão >= 20.10.8]
- [Docker-compose](https://docs.docker.com/compose/compose-file/) [Versão >= 1.29.2]

## Executando o projeto

Utilizamos o docker-compose para executar toda a infraestrutura do projeto, com ele são iniciados os servicos de banco de dados com o Postgres, o ElasticSearch, o backend e o frontend do projeto. Caso deseja executar somente uma parte do projeto, vc pode fazer isso especificando qual no comando do docker-compose.

1. Se certifique de ter Docker, Java e Docker-compose instalados;
2. Clone o projeto;
3. Rode `docker-compose up` ou `make run` para rodar o projeto;
4. Navegue para `localhost:8080/swagger-ui` para ver as opções de comandos disponíveis;
5. o frontend do projeto está sendo executado na porta `3000` do localhost.

## Erros Comuns

No Ubunto versão 20.04 ou superior, indicamos utilizar o docker-compose versão = 2.0.1  

## Contribuir
Você quer contribuir com nosso projeto? É simples! Temos um [guia de contribuição](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/docs/CONTRIBUTING.md) onde são explicados todos os passos para contribuir. Ahh, não esquece de ler nosso [código de conduta](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/docs/CODE_OF_CONDUCT.md).   
Caso reste duvidas você também pode entrar em contato conosco criando uma issue.  
Para contribuir com os nossos projetos acesse nossa organização: [MonitorPolitico](https://gitlab.com/poo-monitor-poltico).



