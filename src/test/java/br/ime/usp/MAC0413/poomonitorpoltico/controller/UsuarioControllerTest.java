package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Deputado;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Partido;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.UsuarioRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UsuarioControllerTest {

    private UsuarioController usuarioController;
    private  DeputadoController deputadoController;
    private PartidoController partidoController;
    @Mock
    private UsuarioRepository usuarioRepository;
    private AutoCloseable closeable;

    @BeforeEach
    public void before() {
        closeable = MockitoAnnotations.openMocks(this);
        usuarioController = new UsuarioController(null, null, null, null, null, usuarioRepository);
    }

    @AfterEach
    public void after() throws Exception {
        closeable.close();
    }

    @Test
    public void createNewUserTest() {
        Usuario usuario = new Usuario();
        usuario.setEmail("test@test.com");

        Mockito.when(usuarioRepository.findUserByEmail(usuario.getEmail())).thenReturn(Optional.empty());

        usuarioController.createUsuario(usuario);
    }

    @Test
    public void createExistingUserTest() {
        Usuario usuario = new Usuario();
        usuario.setEmail("test@test.com");

        Mockito.when(usuarioRepository.findUserByEmail(usuario.getEmail())).thenReturn(Optional.of(usuario));

        assertThrows(IllegalArgumentException.class, () -> {
            usuarioController.createUsuario(usuario);
        });
    }

    @Test
    public void updateNewUserTest() {
        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> {
            usuarioController.updateUsuario(1l, "x", "y", "z");
        });
    }

    @Test
    public void updateExistingUserTest() {
        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(new Usuario()));

        usuarioController.updateUsuario(1l, "x", "y", "z");
        usuarioController.updateUsuario(1l, "", "", "");
        usuarioController.updateUsuario(1l, null, null, null);
    }

    @Test
    public void addNewDeputadoTest() {
        Deputado d = new Deputado();
        d.setId(1);
        Usuario u = new Usuario();
        List deputados = new ArrayList<Deputado>();
        deputados.add(d);
        u.setDeputadosMonitorados(deputados);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));

        ArgumentCaptor<Usuario> userCaptor = ArgumentCaptor.forClass(Usuario.class);
        Mockito.when(usuarioRepository.save(userCaptor.capture())).thenReturn(u);



        deputadoController.adicionaDeputadoMonitorado(1l, 1);

        Usuario result = userCaptor.getValue();
        assertEquals(2, result.getDeputadosMonitorados().size());
        assertEquals(1, result.getDeputadosMonitorados().get(0).getId());
        assertEquals(2, result.getDeputadosMonitorados().get(1).getId());
    }

    @Test
    public void removeDeputadoTest() {
        Deputado d = new Deputado();
        d.setId(1);
        Usuario u = new Usuario();
        List deputados = new ArrayList<Deputado>();
        deputados.add(d);
        u.setDeputadosMonitorados(deputados);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));

        ArgumentCaptor<Usuario> userCaptor = ArgumentCaptor.forClass(Usuario.class);
        Mockito.when(usuarioRepository.save(userCaptor.capture())).thenReturn(u);


        deputadoController.removeDeputadoMonitorado(1l, d);

        Usuario result = userCaptor.getValue();
        assertEquals(0, result.getDeputadosMonitorados().size());

        ResponseEntity res = deputadoController.removeDeputadoMonitorado(1l, d);
        assertEquals(HttpStatus.NOT_FOUND, res.getStatusCode());
    }

    @Test
    public void addExistingDeputadoTest() {
        Deputado d = new Deputado();
        d.setId(1);
        Usuario u = new Usuario();
        List deputados = new ArrayList<Deputado>();
        deputados.add(d);
        u.setDeputadosMonitorados(deputados);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));



        ResponseEntity resp = deputadoController.adicionaDeputadoMonitorado(1l, 1);
        assertEquals(HttpStatus.CONFLICT, resp.getStatusCode());
    }

    @Test
    public void addNewPartidoTest() {
        Partido d = new Partido(1l);
        Usuario u = new Usuario();
        List partidos = new ArrayList<Partido>();
        partidos.add(d);
        u.setPartidosMonitorados(partidos);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));

        ArgumentCaptor<Usuario> userCaptor = ArgumentCaptor.forClass(Usuario.class);
        Mockito.when(usuarioRepository.save(userCaptor.capture())).thenReturn(u);



        partidoController.adicionaPartidoMonitorado(1l, d);

        Usuario result = userCaptor.getValue();
        assertEquals(2, result.getPartidosMonitorados().size());
        assertEquals(1, result.getPartidosMonitorados().get(0).getId());
        assertEquals(2, result.getPartidosMonitorados().get(1).getId());
    }

    @Test
    public void addExistingPartidoTest() {
        Partido d = new Partido(1l);
        Usuario u = new Usuario();
        List partidos = new ArrayList<Partido>();
        partidos.add(d);
        u.setPartidosMonitorados(partidos);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));



        ResponseEntity resp = partidoController.adicionaPartidoMonitorado(1l, d);
        assertEquals(HttpStatus.CONFLICT, resp.getStatusCode());
    }

    @Test
    public void removePartidoTest() {
        Partido d = new Partido(1l);
        Usuario u = new Usuario();
        List partidos = new ArrayList<Partido>();
        partidos.add(d);
        u.setPartidosMonitorados(partidos);

        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.of(u));

        ArgumentCaptor<Usuario> userCaptor = ArgumentCaptor.forClass(Usuario.class);
        Mockito.when(usuarioRepository.save(userCaptor.capture())).thenReturn(u);

        partidoController.removePartidoMonitorado(1l,1);

        Usuario result = userCaptor.getValue();
        assertEquals(0, result.getPartidosMonitorados().size());

        ResponseEntity resp = partidoController.removePartidoMonitorado(1l,1);
        assertEquals(HttpStatus.NOT_FOUND, resp.getStatusCode());
    }

    @Test
    public void notFound() {
        Partido p = new Partido(1l);
        Mockito.when(usuarioRepository.findById(1l)).thenReturn(Optional.empty());


        ResponseEntity resp = partidoController.adicionaPartidoMonitorado(1l, p);
        assertEquals(HttpStatus.NOT_FOUND, resp.getStatusCode());

        resp = partidoController.removePartidoMonitorado(1l,1);
        assertEquals(HttpStatus.NOT_FOUND, resp.getStatusCode());

        Deputado d = new Deputado();
        d.setId(1);

        resp = deputadoController.adicionaDeputadoMonitorado(1l, 1);
        assertEquals(HttpStatus.NOT_FOUND, resp.getStatusCode());

        resp = deputadoController.removeDeputadoMonitorado(1l, d);
        assertEquals(HttpStatus.NOT_FOUND, resp.getStatusCode());

        assertThrows(IllegalArgumentException.class, () -> {
            usuarioController.updateUsuario(1l, "test", "test 2", "test 3");
        });
    }

    @Test
    public void loginTest() {
        Mockito.when(usuarioRepository.findUserByEmail("test")).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> {
            usuarioController.login("test", "steste");
        });

        Usuario usuario = new Usuario(1l);
        usuario.setSenha("steste");

        Mockito.when(usuarioRepository.findUserByEmail("test")).thenReturn(Optional.of(usuario));

        assertEquals(true, usuarioController.login("test", "steste"));
        assertEquals(false, usuarioController.login("test", "teste"));
    }
}
