package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Deputado;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.DeputadoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeputadoControllerTest {
    @Mock
    private DeputadoRepository repository;
    private DeputadoController controller;
    private AutoCloseable closeable;

    @BeforeEach
    public void before() {
        closeable = MockitoAnnotations.openMocks(this);
        controller = new DeputadoController(repository);
    }

    @AfterEach
    public void after() throws Exception {
        closeable.close();
    }

    @Test
    public void getDeputadosTest() {
        List<Deputado> deps = new ArrayList<Deputado>();
        Deputado d1 = new Deputado();
        d1.setNome("teste 1");
        d1.setLocalidade("Osasco");
        deps.add(d1);

        Deputado d2 = new Deputado();
        d2.setNome("teste 2");
        d2.setGenero("genero");
        deps.add(d2);

        List<Deputado> deps1 = new ArrayList<Deputado>();
        deps1.add(d1);

        Mockito.when(repository.findAll()).thenReturn(deps);
        Mockito.when(repository.findByNome(Mockito.anyString())).thenReturn(deps1);

        ResponseEntity<List<Deputado>> resp = controller.getDeputados(null);
        assertEquals(2, resp.getBody().size());
        assertEquals("genero", resp.getBody().get(1).getGenero());

        resp = controller.getDeputados("");
        assertEquals(2, resp.getBody().size());

        resp = controller.getDeputados("teste 1");
        assertEquals(1, resp.getBody().size());
        assertEquals("Osasco", resp.getBody().get(0).getLocalidade());
    }
}
