package br.ime.usp.MAC0413.poomonitorpoltico.service;

import br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository.ProposicaoElasticRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProposicaoService {
    private final ProposicaoElasticRepository repository;

    @Autowired
    public ProposicaoService(ProposicaoElasticRepository repository) {
        this.repository = repository;
    }

    public void save(final Proposicao proposicao) {
        repository.save(proposicao);
    }

    public Proposicao findById(final Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Proposicao> findAll() {
        return repository.findAll();
    }

}
