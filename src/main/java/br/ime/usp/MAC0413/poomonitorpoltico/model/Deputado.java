package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;

@Entity(name = "Deputado")
@Table(name = "modelagem_parlamentar")
public class Deputado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "parlamentar_id")
    private String parlamentar_id;

    @Column(name = "partido_id")
    private int partidoId;

    @Column(name = "nome")
    private String nome;

    @Column(name = "casa_legislativa_id")
    private long casa_legislativa_id;

    @Column(name = "genero")
    private String genero;

    @Column(name = "localidade")
    private String localidade;

    public Deputado() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getParlamentar_id() {
        return parlamentar_id;
    }

    public int getPartido_id() {
        return partidoId;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public long getCasaLegislativaId() {
        return casa_legislativa_id;
    }

    public void setCasaLegislativaId(long id) {
        this.casa_legislativa_id = id;
    }

    public void setPartidoId(int id) {
        this.partidoId = id;
    }

    public void setParlamentarId(String id) {
        this.parlamentar_id = id;
    }

    @Override
    public String toString() {
        return "empty";
    }

}
