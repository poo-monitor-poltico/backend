package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Votacao;
import org.springframework.beans.factory.annotation.Autowired;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Voto;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.VotacaoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.VotoRepository;

import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/votacao")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VotacaoController {
    @Autowired
    private VotacaoRepository votacaoRepository;
    @Autowired
    private VotoRepository votoRepository;

    @GetMapping("/somaTotal")
    @ResponseStatus(HttpStatus.OK)
    public int[] getSomaVotos(@RequestParam int id) {
        List<Votacao> votacoes = votacaoRepository.findByProposicaoId(id);
        int[] somas = new int[3];

        for (Votacao votacao : votacoes) {
            List<Voto> votos = votoRepository.findAllByVotacaoId(votacao.getId());
            for (Voto v : votos) {
                if (v.getOpcao().equals("SIM")) {
                    somas[0]++;
                } else if (v.getOpcao().equals("NAO")) {
                    somas[1]++;
                } else {
                    somas[2]++;
                }
            }
        }
        return somas;
    }
}
