package br.ime.usp.MAC0413.poomonitorpoltico.load;

import br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository.ProposicaoElasticRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

public class Loaders {
    @Autowired
    ProposicaoRepository proposicaoRepository;

    @Autowired
    ProposicaoElasticRepository proposicaoElasticRepository;

    @PostConstruct
    @Transactional
    public void loadAll() {
        List<Proposicao> data = proposicaoRepository.findAll();
        proposicaoElasticRepository.saveAll(data);
    }
}
