package br.ime.usp.MAC0413.poomonitorpoltico.model;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

import java.util.Objects;

import br.ime.usp.MAC0413.poomonitorpoltico.helper.Indices;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;

@Document(indexName = Indices.PROPOSICAO_INDEX)
@Entity
@Table(name = "modelagem_proposicao")
public class Proposicao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Field(type = FieldType.Keyword)
    private int id;

    @Field(type = FieldType.Text)
    @Column(name = "id_prop")
    private String id_prop;

    @Field(type = FieldType.Text)
    @Column(name = "sigla")
    private String sigla;

    @Field(type = FieldType.Text)
    @Column(name = "numero")
    private String numero;

    @Field(type = FieldType.Text)
    @Column(name = "ano")
    private String ano;

    @Field(type = FieldType.Text)
    @Column(name = "ementa")
    private String ementa;

    @Field(type = FieldType.Text)
    @Column(name = "descricao")
    private String descricao;

    @Field(type = FieldType.Text)
    @Column(name = "indexacao")
    private String indexacao;

    @Field(type = FieldType.Date)
    @Column(name = "data_apresentacao")
    private Date data_apresentacao;

    @Field(type = FieldType.Text)
    @Column(name = "situacao")
    private String situacao;

    @Field(type = FieldType.Text)
    @Column(name = "autor_principal")
    private String autor_principal;

    @Field(type = FieldType.Integer)
    @Column(name = "casa_legislativa_id")
    private int casa_legislativa_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getId_prop() {
        return id_prop;
    }

    public void setId_prop(String id_prop) {
        this.id_prop = id_prop;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getIndexacao() {
        return indexacao;
    }

    public void setIndexacao(String indexacao) {
        this.indexacao = indexacao;
    }

    public Date getData_apresentacao() {
        return data_apresentacao;
    }

    public void setData_apresentacao(Date data_apresentacao) {
        this.data_apresentacao = data_apresentacao;
    }

    public int getCasa_legislativa_id() {
        return casa_legislativa_id;
    }

    public void setCasa_legislativa_id(int casa_legislativa_id) {
        this.casa_legislativa_id = casa_legislativa_id;
    }

    public String getAutor_principal() {
        return autor_principal;
    }

    public void setAutor_principal(String autor_principal) {
        this.autor_principal = autor_principal;
    }

    @Override
    public String toString() {
        return "IdProposicao = " + id + " ID_prop = " + id_prop + " ano = " + ano + " sigla" + sigla+" indexacao "+indexacao;
    }


}
