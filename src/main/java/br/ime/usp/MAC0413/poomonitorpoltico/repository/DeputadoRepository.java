package br.ime.usp.MAC0413.poomonitorpoltico.repository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Deputado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeputadoRepository extends JpaRepository<Deputado, Long> {
    List<Deputado> findByNome(String nome);

    List<Deputado> findByPartidoId(int idPartido);

    List<Deputado> findByIdIn(List<Integer> ids);
    
}
