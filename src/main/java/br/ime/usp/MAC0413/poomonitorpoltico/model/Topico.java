package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;

@Entity(name = "Topico")
@Table(name = "modelagem_topico")
public class Topico {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "expressao_chave")
    private String expressao_chave;

    @Column(name = "regex")
    private boolean regex;

    public Topico() {
    }

    public Topico(String expressao, boolean regex) {
        this.expressao_chave = expressao;
        this.regex = regex;
    }

    public Long getId() {
        return id;
    }

    public String getExpressao_chave() {
        return expressao_chave;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isRegex() {
        return regex;
    }

    public void setRegex(boolean regex) {
        this.regex = regex;
    }

    public void setExpressao_chave(String expressao_chave) {
        this.expressao_chave = expressao_chave;
    }

}
