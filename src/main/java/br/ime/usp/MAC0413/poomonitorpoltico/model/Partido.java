package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;

@Entity(name = "Partido")
@Table(name = "modelagem_partido")
public class Partido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nome;

    @Column
    private int numero;

    @Column
    private String cor;

    public Partido(long id) {
        this.id = id;
    }

    public Partido() {

    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}
