package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.*;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.util.*;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class UsuarioController {

    @Autowired
    private final UsuarioRepository usuarioRepository;
    @Autowired
    private final VotoRepository votoRepository;
    @Autowired
    private final ProposicaoRepository proposicaoRepository;
    @Autowired
    private final DeputadoRepository deputadoRepository;
    @Autowired
    private final VotacaoRepository votacaoRepository;
    @Autowired
    private final TopicoRepository topicoRepository;

    public UsuarioController(
            TopicoRepository topicorepo,
            VotacaoRepository votarepo,
            DeputadoRepository depurepo,
            ProposicaoRepository proprepo,
            VotoRepository votorepo,
            UsuarioRepository repo) {
        usuarioRepository = repo;
        votoRepository = votorepo;
        proposicaoRepository = proprepo;
        deputadoRepository = depurepo;
        votacaoRepository = votarepo;
        topicoRepository = topicorepo;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario> getUsuarios() {
        return usuarioRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Usuario getUsuario(@PathVariable Long id) {
        return usuarioRepository.findById(id).get();
    }

    @GetMapping("/{id}/topicos")
    @ResponseStatus(HttpStatus.OK)
    public List<Topico> getTopicos(@PathVariable Long id) {
        return usuarioRepository.findById(id).get().getTopicosMonitorados();
    }

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUsuario(@RequestBody @Valid Usuario usuario) {
        Optional<Usuario> userOptional = usuarioRepository.findUserByEmail(usuario.getEmail());
        if (userOptional.isPresent()) {
            throw new IllegalArgumentException("Email já cadastrado");
        }
        usuarioRepository.save(usuario);
    }

    @DeleteMapping("/{id}")
    public void deleteUsuario(@PathVariable Long id) {
        usuarioRepository.deleteById(id);
    }

    @PutMapping("/{id}")
    public void updateUsuario(
            @PathVariable Long id,
            @RequestParam(required = false) String nome,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String senha) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (!usuarioOptional.isPresent()) {
            throw new IllegalArgumentException("Usuário não cadastrado");
        }
        Usuario usuario = usuarioOptional.get();
        if (nome != null && nome.length() > 0 && !Objects.equals(usuario.getNome(), nome)) {
            usuario.setNome(nome);
            usuarioRepository.save(usuario);
        }
        if (email != null && email.length() > 0 && !Objects.equals(usuario.getEmail(), email)) {
            usuario.setEmail(email);
            usuarioRepository.save(usuario);
        }
        if (senha != null && senha.length() > 0 && !Objects.equals(usuario.getSenha(), senha)) {
            usuario.setSenha(senha);
            usuarioRepository.save(usuario);
        }
    }

    @PostMapping("login")
    public boolean login(
            @RequestParam String email,
            @RequestParam String senha) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findUserByEmail(email);
        if (!usuarioOptional.isPresent()) {
            throw new IllegalArgumentException("Usuário não cadastrado");
        }
        Usuario usuario = usuarioOptional.get();
        return usuario.getSenha().equals(senha);
    }

}
