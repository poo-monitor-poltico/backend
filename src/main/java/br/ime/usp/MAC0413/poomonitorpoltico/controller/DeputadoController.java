package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Deputado;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.DeputadoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/deputado")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DeputadoController {

    @Autowired
    private DeputadoRepository deputadoRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    public DeputadoController(DeputadoRepository repo) {
        deputadoRepository = repo;
    }

    @GetMapping
    public ResponseEntity<List<Deputado>> getDeputados(@RequestParam(name = "nome", required = false) String nome) {

        try {
            List<Deputado> deputados = new ArrayList<>();
            if (nome == null || nome == "") {
                deputadoRepository.findAll().forEach(deputados::add);
            } else {
                deputadoRepository.findByNome(nome).forEach(deputados::add);
            }

            if (deputados.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(deputados, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{partidoId}")
    public ResponseEntity<List<Deputado>> getDeputado(@PathVariable int partidoId) {
        return new ResponseEntity<>(deputadoRepository.findByPartidoId(partidoId), HttpStatus.OK);
    }


    @PutMapping("/removeDeputadoMonitorado")
    public ResponseEntity<Usuario> adicionaDeputadoMonitorado(@RequestParam("userid") long id, @RequestParam int deputadoId) {
        Optional<Usuario> userData = usuarioRepository.findById(id);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario _usuario = userData.get();
        List<Deputado> monitorados = _usuario.getDeputadosMonitorados();
        if (monitorados.removeIf(dep -> dep.getId() == deputadoId)) {
            _usuario.setDeputadosMonitorados(monitorados);
            return new ResponseEntity<>(usuarioRepository.save(_usuario), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/adicionaDeputadoMonitorado")
    public ResponseEntity<Usuario> removeDeputadoMonitorado(@RequestParam("userid") long id, @RequestBody Deputado deputado) {
        Optional<Usuario> userData = usuarioRepository.findById(id);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario _usuario = userData.get();
        List<Deputado> monitorados = _usuario.getDeputadosMonitorados();

        //verificar se já existe o deputado na lista
        if (monitorados.stream().anyMatch(d -> d.getId() == deputado.getId())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        monitorados.add(deputado);
        _usuario.setDeputadosMonitorados(monitorados);
        return new ResponseEntity<>(usuarioRepository.save(_usuario), HttpStatus.OK);
    }
}
