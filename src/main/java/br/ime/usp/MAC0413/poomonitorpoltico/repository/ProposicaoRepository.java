package br.ime.usp.MAC0413.poomonitorpoltico.repository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProposicaoRepository extends JpaRepository<Proposicao, Integer> {

    @Transactional
    @Modifying
    @Query(value = "SELECT p from Proposicao p WHERE p.id IN (SELECT vot.proposicaoId from Votacao vot where vot.id IN (:votacoes))")
    List<Proposicao> findAll(@Param("votacoes") List<Integer> votacoes);

    Optional<Proposicao> findById(Integer id);
}

