package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Votacao")
@Table(name = "modelagem_Votacao")
public class Votacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String id_vot;
    @Column
    private String descricao;
    @Column
    private String resultado;
    @Column
    private Date data;
    @Column
    private int proposicaoId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_vot() {
        return id_vot;
    }

    public void setId_vot(String id_vot) {
        this.id_vot = id_vot;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getProposicaoId() {
        return proposicaoId;
    }

    public void setProposicaoId(int proposicaoId) {
        this.proposicaoId = proposicaoId;
    }

    @Override
    public String toString() {
        return "id= " + id + " proposicao_id = " + proposicaoId;
    }
}
