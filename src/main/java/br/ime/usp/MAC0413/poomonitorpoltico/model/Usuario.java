package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "User")
@Table(name = "modelagem_usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "nome")
    @NotBlank
    private String nome;

    @Column(unique = true, nullable = false, name = "email")
    @Email
    private String email;

    @Column(nullable = false, name = "senha")
    private String senha;

    @ManyToMany
    private List<Partido> partidosMonitorados;
    @ManyToMany
    private List<Deputado> deputadosMonitorados;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Topico> topicosMonitorados;

    public Usuario(Long id) {
        this.id = id;

        partidosMonitorados = new ArrayList<Partido>();
        deputadosMonitorados = new ArrayList<Deputado>();
    }

    public Usuario() {
        partidosMonitorados = new ArrayList<Partido>();
        deputadosMonitorados = new ArrayList<Deputado>();
    }

    public List<Topico> getTopicosMonitorados() {
        return topicosMonitorados;
    }

    public void setTopicosMonitorados(List<Topico> topicosMonitorados) {
        this.topicosMonitorados = topicosMonitorados;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Partido> getPartidosMonitorados() {
        return partidosMonitorados;
    }

    public void setPartidosMonitorados(List<Partido> partidosMonitorados) {
        this.partidosMonitorados = partidosMonitorados;
    }

    public List<Deputado> getDeputadosMonitorados() {
        return deputadosMonitorados;
    }

    public void setDeputadosMonitorados(List<Deputado> deputadosMonitorados) {
        this.deputadosMonitorados = deputadosMonitorados;
    }
}
