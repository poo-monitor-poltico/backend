package br.ime.usp.MAC0413.poomonitorpoltico.repository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Voto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface VotoRepository extends JpaRepository<Voto, Long> {
    @Query(value = "SELECT v from Voto v where " +
            "v.parlamentarId IN (:deputados) AND " +
            "v.votacaoId IN (SELECT vot FROM Votacao vot WHERE " +
            "vot.id=v.votacaoId AND vot.data BETWEEN :dataInicio AND :dataFim)")
    List<Voto> findAllVotosByDeputados(@Param("deputados") List<Integer> deputados, @Param("dataInicio") Date dataInicio, @Param("dataFim") Date dataFim);

    @Query(value = "SELECT v from Voto v WHERE v.parlamentarId IN (SELECT d.id from Deputado d where d.partidoId IN (:partidos)) AND v.votacaoId IN (SELECT vot FROM Votacao vot WHERE vot.id=v.votacaoId AND vot.data BETWEEN :dataInicio AND :dataFim)")
    List<Voto> findAllVotosByPartidos(@Param("partidos") List<Integer> partidos, @Param("dataInicio") Date dataInicio, @Param("dataFim") Date dataFim);
    List<Voto> findAllByVotacaoId(Integer id);
    List<Voto> findByParlamentarId(Integer deputadoId);
    @Query(value = "SELECT v from Voto v WHERE v.parlamentarId IN (SELECT d.id from Deputado d where d.partidoId=:partido)")
    List<Voto> findAllVotosByPartido(@Param("partido") Integer partido);

}
