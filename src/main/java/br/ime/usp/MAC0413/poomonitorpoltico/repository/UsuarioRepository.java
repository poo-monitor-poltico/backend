package br.ime.usp.MAC0413.poomonitorpoltico.repository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query("SELECT u FROM User u WHERE u.email = ?1")
    Optional<Usuario> findUserByEmail(String email);
    //@Query(value="SELECT v FROM  Voto v  WHERE v.parlamentar_id IN ?1 AND v.votacao_id IN (select vot FROM Votacao vot WHERE v.votacao_id=:vot.id AND vot.data BETWEEN ?2 AND ?3)")
    //List<Voto> findByParlamentarIdInAndDataBetween(List<Integer> parlamentares,Date dataInicio,Date dataFim);
}
