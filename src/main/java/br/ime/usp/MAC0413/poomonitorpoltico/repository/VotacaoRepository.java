package br.ime.usp.MAC0413.poomonitorpoltico.repository;


import br.ime.usp.MAC0413.poomonitorpoltico.model.Votacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.*;
@Repository
public interface VotacaoRepository extends JpaRepository<Votacao, Integer> {

    @Query(value = "SELECT v from Votacao v WHERE v.proposicaoId IN (:votacoes)")
    List<Votacao> findAll(@Param("votacoes") List<Integer> votacoes);
    List<Votacao> findByProposicaoId(Integer id);
    Optional findById(Integer id);
}
