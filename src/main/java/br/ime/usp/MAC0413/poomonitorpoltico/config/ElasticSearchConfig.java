package br.ime.usp.MAC0413.poomonitorpoltico.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "br.ime.usp.MAC0413.poomonitorpoltico.repository")
@EnableElasticsearchRepositories(basePackages = "br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository")
@ComponentScan(basePackages = {"br.ime.usp.MAC0413.poomonitorpoltico"})
public class ElasticSearchConfig extends AbstractElasticsearchConfiguration {
    @Bean
    @Override
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo("10.5.0.5:9200")
                .build();

        return RestClients.create(clientConfiguration).rest();
    }
}
