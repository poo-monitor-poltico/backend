package br.ime.usp.MAC0413.poomonitorpoltico;

import br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository.ProposicaoElasticRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@SpringBootApplication
public class PooMonitorPolticoApplication {

    @Autowired
    private ElasticsearchOperations esOps;

    @Autowired
    private ProposicaoElasticRepository proposicaoElasticRepository;

    @Autowired
    private ProposicaoRepository proposicaoRepository;

    public static void main(String[] args) {
        SpringApplication.run(PooMonitorPolticoApplication.class, args);
    }

    @PreDestroy
    public void deleteIndex() {
        esOps.indexOps(Proposicao.class).delete();
    }

    @PostConstruct
    public void buildIndex() {

        esOps.indexOps(Proposicao.class).refresh();
        proposicaoElasticRepository.deleteAll();
        proposicaoElasticRepository.saveAll(proposicaoRepository.findAll());
    }

}
