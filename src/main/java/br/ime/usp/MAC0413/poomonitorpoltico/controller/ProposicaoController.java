package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository.ProposicaoElasticRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RegexpQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Deputado;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository.ProposicaoElasticRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.helper.Indices;
import br.ime.usp.MAC0413.poomonitorpoltico.service.ProposicaoService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

import java.util.ArrayList;

@RestController
@RequestMapping("/proposicao")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProposicaoController {

    private static final String PROPOSICAO_INDEX = "proposicaoindex";

    @Autowired
    private ProposicaoRepository proposicaoRepository;
    @Autowired
    private ProposicaoElasticRepository proposicaoElasticRepository;
    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    public ProposicaoController(ProposicaoRepository repo, ProposicaoElasticRepository elasticrepo,
            ElasticsearchOperations elasticops) {
        proposicaoRepository = repo;
        proposicaoElasticRepository = elasticrepo;
        elasticsearchOperations = elasticops;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Proposicao getProposicao(@PathVariable int id) {
        Optional<Proposicao> proposicaoEntity = proposicaoRepository.findById(id);
        if (proposicaoEntity.isPresent()) {
            Proposicao p = proposicaoEntity.get();
            return p;
        } else {
            return new Proposicao();
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Proposicao createProposicao(@RequestBody Proposicao proposicao) {
        return proposicaoRepository.save(proposicao);
    }

    @DeleteMapping("/{id}")
    public void deleteProposicao(@PathVariable int id) {
        proposicaoRepository.deleteById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Proposicao> getProposicoes() {
        List<Proposicao> listaProposicoes = proposicaoRepository.findAll();
        return listaProposicoes;
    }

    @GetMapping("/Elastic/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Proposicao> getProposicoesElastic() {
        return proposicaoElasticRepository.findAll();
    }

    /**
     * Busca proposições de acordo com uma query,
     * que pode filtrar por ementa, descrição ou index
     * 
     * @param query para ElasticSearch
     * @return lista de proposições retornadas
     */
    @GetMapping("/elasticQuery")
    @ResponseBody
    public List<Proposicao> fetchByEmentaOrDescOrIndex(
            @RequestParam(name = "q", required = false) String query,
            @RequestParam boolean regex) {

        if (regex) {
            RegexpQueryBuilder regexpQueryBuilder = new RegexpQueryBuilder("descricao", query);

            Query searchQuery = new NativeSearchQueryBuilder()
                    .withFilter(regexpQueryBuilder)
                    .build();

            // Executa busca
            SearchHits<Proposicao> proposicaoHits = elasticsearchOperations
                    .search(searchQuery, Proposicao.class,
                            IndexCoordinates.of(Indices.PROPOSICAO_INDEX));

            // Mapeia searchHits para lista de proposicoes
            List<Proposicao> proposicaoMatches = new ArrayList<Proposicao>();
            proposicaoHits.forEach(srchHit -> {
                proposicaoMatches.add(srchHit.getContent());
            });
            return proposicaoMatches;

        } else {
            // Cria query com varios campos para habilitar fuzzy search
            QueryBuilder queryBuilder = QueryBuilders
                    .multiMatchQuery(query, "ementa", "descricao", "indexacao")
                    .fuzziness(Fuzziness.AUTO);

            Query searchQuery = new NativeSearchQueryBuilder()
                    .withFilter(queryBuilder)
                    .build();

            // Executa busca
            SearchHits<Proposicao> proposicaoHits = elasticsearchOperations
                    .search(searchQuery, Proposicao.class,
                            IndexCoordinates.of(Indices.PROPOSICAO_INDEX));

            // Mapeia searchHits para lista de proposicoes
            List<Proposicao> proposicaoMatches = new ArrayList<Proposicao>();
            proposicaoHits.forEach(srchHit -> {
                proposicaoMatches.add(srchHit.getContent());
            });
            return proposicaoMatches;

        }

    }

}
