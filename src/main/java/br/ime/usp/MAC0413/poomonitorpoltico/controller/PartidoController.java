package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Partido;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.PartidoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/partido")
public class PartidoController {

    @Autowired
    private final PartidoRepository partidoRepository;
    @Autowired
    private final UsuarioRepository usuarioRepository;
    public PartidoController(PartidoRepository repo,UsuarioRepository urepo) {
        partidoRepository = repo;
        usuarioRepository =urepo;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Partido> getPartidos() {
        return partidoRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Partido getPartido(@PathVariable Long id) {
        return partidoRepository.findById(id).get();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Partido createPartido(@RequestBody Partido partido) {
        return partidoRepository.save(partido);
    }

    @DeleteMapping("/{id}")
    public void deletePartido(@PathVariable Long id) {
        partidoRepository.deleteById(id);
    }

    @PutMapping()
    public Partido updatePartido(@RequestBody Partido partido) {
        return partidoRepository.save(partido);
    }
    @PutMapping("/removePartido")
    public ResponseEntity<Usuario> removePartidoMonitorado(@RequestParam("userid") long id, @RequestParam("partidoId") int partidoId) {
        Optional<Usuario> userData = usuarioRepository.findById(id);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario _usuario = userData.get();
        List<Partido> monitorados = _usuario.getPartidosMonitorados();
        if (monitorados.removeIf(part -> part.getId() == partidoId)) {
            _usuario.setPartidosMonitorados(monitorados);
            return new ResponseEntity<>(usuarioRepository.save(_usuario), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/adicionaPartido")
    public ResponseEntity<Usuario> adicionaPartidoMonitorado(@RequestParam("userid") Long id, @RequestBody Partido partido) {
        Optional<Usuario> userData = usuarioRepository.findById(id);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario _usuario = userData.get();
        List<Partido> monitorados = _usuario.getPartidosMonitorados();

        //verificar se já existe o deputado na lista
        if (monitorados.stream().anyMatch(d -> d.getId() == partido.getId())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        monitorados.add(partido);
        _usuario.setPartidosMonitorados(monitorados);
        return new ResponseEntity<>(usuarioRepository.save(_usuario), HttpStatus.OK);
    }
}
