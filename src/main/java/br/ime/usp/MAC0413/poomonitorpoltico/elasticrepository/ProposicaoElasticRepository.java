package br.ime.usp.MAC0413.poomonitorpoltico.elasticrepository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Proposicao;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;


public interface ProposicaoElasticRepository extends ElasticsearchRepository<Proposicao, Long> {
    List<Proposicao> findAll();
}
