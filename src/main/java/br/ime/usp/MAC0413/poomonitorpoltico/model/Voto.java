package br.ime.usp.MAC0413.poomonitorpoltico.model;

import javax.persistence.*;

@Entity(name = "Voto")
@Table(name = "modelagem_voto")
public class Voto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private int parlamentarId;
    @Column
    private int votacaoId;
    @Column
    private String opcao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getParlamentarId() {
        return parlamentarId;
    }

    public void setParlamentarId(int parlamentarId) {
        this.parlamentarId = parlamentarId;
    }

    public int getVotacaoId() {
        return votacaoId;
    }

    public void setVotacaoId(int votacaoId) {
        this.votacaoId = votacaoId;
    }

    public String getOpcao() {
        return opcao;
    }

    public void setOpcao(String opcao) {
        this.opcao = opcao;
    }

    @Override
    public String toString() {
        return "ParalamentarId = " + parlamentarId + " votacaoId = " + votacaoId + " opcao = " + opcao;
    }
}
