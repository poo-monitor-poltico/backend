package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Topico;
import br.ime.usp.MAC0413.poomonitorpoltico.model.Usuario;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.TopicoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/topico")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TopicoController {

    @Autowired
    private TopicoRepository topicoRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    public TopicoController(TopicoRepository repo) {
        topicoRepository = repo;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Topico> getTopicos() {
        return topicoRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Topico getTopico(@PathVariable Long id) {
        return topicoRepository.findById(id).get();
    }

    @PutMapping("/adicionaTopico")
    public void monitoraTopico(
            @RequestParam("idUsuario") long id,
            @RequestParam String expressao,
            @RequestParam boolean regex) {

        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);

        if (usuarioOptional.isPresent()) {
            Usuario usuario = usuarioOptional.get();

            List<Topico> monitorados = usuario.getTopicosMonitorados();
            Topico topico = new Topico(expressao.toLowerCase(), regex);
            topicoRepository.save(topico);
            monitorados.add(topico);
            usuario.setTopicosMonitorados(monitorados);
            usuarioRepository.save(usuario);
        }
    }

    @PutMapping("/removeTopico")
    public void removeTopico(
            @RequestParam("idUsuario") long id,
            @RequestParam Long topicoId) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);

        if (usuarioOptional.isPresent()) {
            Usuario usuario = usuarioOptional.get();

            List<Topico> monitorados = usuario.getTopicosMonitorados();

            if (monitorados.removeIf(top -> top.getId() == topicoId)) {
                usuario.setTopicosMonitorados(monitorados);
                usuarioRepository.save(usuario);
            }
        }
    }

}
