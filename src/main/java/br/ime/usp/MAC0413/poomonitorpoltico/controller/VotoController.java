package br.ime.usp.MAC0413.poomonitorpoltico.controller;

import antlr.ASTNULLType;
import br.ime.usp.MAC0413.poomonitorpoltico.model.*;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.ProposicaoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.VotacaoRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.UsuarioRepository;
import br.ime.usp.MAC0413.poomonitorpoltico.repository.VotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.*;
@RestController
@RequestMapping("/voto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VotoController {
    @Autowired
    private VotoRepository votoRepository;
    @Autowired
    private ProposicaoRepository proposicaoRepository;
    @Autowired
    private VotacaoRepository votacaoRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @GetMapping("/listaVotosPorDeputado")
    public Map<Integer, List<List<Voto>>> votosPorDeputado(@RequestParam("userid") long id,
                                                           @RequestParam Date dataInicio,
                                                           @RequestParam Date dataFim) {

	Optional<Usuario> userData = usuarioRepository.findById(id);
        List<Deputado> listaDeDeputados = userData.get().getDeputadosMonitorados();
        System.out.println(listaDeDeputados.size());
        List<Integer> idsDeputados = getIds(listaDeDeputados);
        List<Voto> votoPorDeputado = votoRepository.findAllVotosByDeputados(idsDeputados, dataInicio, dataFim);
        List<Integer> idsVotacoes = getIds(votoPorDeputado);
        List<Proposicao> proposicoes = proposicaoRepository.findAll(idsVotacoes);
        Map<Integer, List<List<Voto>>> proposicaoVotosMapping = new TreeMap<>();
        List<Integer> idsProposicoes = getIds(proposicoes);

        List<Votacao> votacoes = votacaoRepository.findAll(idsProposicoes);
        for (Integer i : idsProposicoes) {
            proposicaoVotosMapping.put(i, new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
        }
        for (var entry : proposicaoVotosMapping.entrySet()) {
            for (Votacao vot : votacoes) {
                if (entry.getKey() == vot.getProposicaoId()) {
                    for (Voto v : votoPorDeputado) {
                        if (vot.getId() == v.getVotacaoId()) {
                            if (v.getOpcao().equals("SIM")) {
                                entry.getValue().get(0).add(v);
                            } else if (v.getOpcao().equals("NAO")) {
                                entry.getValue().get(1).add(v);
                            } else {
                                entry.getValue().get(2).add(v);
                            }
                        }
                    }
                }
            }
        }
        return proposicaoVotosMapping;
    }

    @GetMapping("/listaVotosPorPartido")
    public Map<Integer, List<List<Voto>>> votosPorPartido(@RequestParam("userid") long id,
                                                          @RequestParam Date dataInicio,
                                                          @RequestParam Date dataFim) {
        Optional<Usuario> userData = usuarioRepository.findById(id);
        
        List<Partido> listaDePartidos = userData.get().getPartidosMonitorados();
        System.out.println(listaDePartidos.size());
        List<Integer> idsPartidos = getIds(listaDePartidos);    
        List<Voto> votoPorPartido = votoRepository.findAllVotosByPartidos(idsPartidos, dataInicio, dataFim);
        List<Integer> idsVotacoes = getIds(votoPorPartido);
        List<Proposicao> proposicoes = proposicaoRepository.findAll(idsVotacoes);
        List<Integer> idsProposicoes = getIds(proposicoes);
        List<Votacao> votacoes = votacaoRepository.findAll(idsProposicoes);

        Map<Integer, List<List<Voto>>> proposicaoVotosMapping = new TreeMap<>();
        for (Integer i : idsProposicoes) {
            proposicaoVotosMapping.put(i, new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
            proposicaoVotosMapping.get(i).add(new ArrayList<>());
        }
        for (var entry : proposicaoVotosMapping.entrySet()) {
            for (Votacao vot : votacoes) {
                if (entry.getKey() == vot.getProposicaoId()) {
                    for (Voto v : votoPorPartido) {
                        if (vot.getId() == v.getVotacaoId()) {
                            if (v.getOpcao().equals("SIM")) {
                                entry.getValue().get(0).add(v);
                            } else if (v.getOpcao().equals("NAO")) {
                                entry.getValue().get(1).add(v);
                            } else {
                                entry.getValue().get(2).add(v);
                            }
                        }
                    }
                }
            }
        }
        return proposicaoVotosMapping;
    }
    private List<Integer> getIds(List<?> entidades) {
        List<Integer> ids = new ArrayList<>();
        if (entidades.get(0) instanceof Deputado) {
            for (int i = 0; i < entidades.size(); i++) {
                ids.add(((Deputado) entidades.get(i)).getId());
            }
        } else if (entidades.get(0) instanceof Partido) {
            for (int i = 0; i < entidades.size(); i++) {
                Partido p = (Partido) entidades.get(i);
                ids.add(Integer.parseInt(p.getId().toString()));
            }
        } else if (entidades.get(0) instanceof Voto) {
            for (int i = 0; i < entidades.size(); i++) {
                if (!entidades.contains(((Voto) entidades.get(i)).getVotacaoId()))
                    ids.add(((Voto) entidades.get(i)).getVotacaoId());
            }
        } else {
            for (int i = 0; i < entidades.size(); i++) {
                ids.add(((Proposicao) entidades.get(i)).getId());
            }
        }
        return ids;
    }
    @GetMapping("/NuvemDeputado")
    public Pair<Map<String,Integer>,Map<String,Integer>> votosPorResposta(@RequestParam("deputado") Integer deputadoId){
        Map<String,Integer> resultadoSim = new TreeMap<String,Integer>();
        Map<String,Integer> resultadoNao = new TreeMap<String,Integer>();
        List<Voto> votos = votoRepository.findByParlamentarId(deputadoId);
        System.out.println(votos.size());

        for(Voto voto : votos){
            if(voto.getOpcao().equals("SIM")){
                int votacaoId = (int)voto.getVotacaoId();
                Votacao votacao= (Votacao)votacaoRepository.findById(votacaoId).get();
                Proposicao proposicao= proposicaoRepository.findById(votacao.getProposicaoId()).get();
                //System.out.println(proposicao);
                String[] palavras = proposicao.getIndexacao().split(",");
                if(palavras.length>3){
		         for(int i=0;i<3;i++){
		            if(resultadoSim.get(palavras[i])!=null){
		                int sum = resultadoSim.get(palavras[i]);
		                resultadoSim.put(palavras[i],sum+1);
		            }else{
		                resultadoSim.put(palavras[i],0);
		            }
		        }               
                }

            }else if(voto.getOpcao().equals("NAO")){
                int votacaoId = voto.getVotacaoId();
                Votacao votacao= (Votacao)votacaoRepository.findById(votacaoId).get();
                Proposicao proposicao= proposicaoRepository.findById(votacao.getProposicaoId()).get();
                String[] palavras = proposicao.getIndexacao().split(",");
                if(palavras.length>3){
		         for(int i=0;i<3;i++){
		            if(resultadoNao.get(palavras[i])!=null){
		                int sum = resultadoNao.get(palavras[i]);
		                resultadoNao.put(palavras[i],sum+1);

		            }else{
		                resultadoNao.put(palavras[i],0);
		            }
		        }	               
                }

            }
        }
        return Pair.of(resultadoSim,resultadoNao);
    }
    @GetMapping("/NuvemPartido")
    public Pair<Map<String,Integer>,Map<String,Integer>> votosPorPartido(@RequestParam("partido") Integer partidoId){
        Map<String,Integer> resultadoSim = new TreeMap<String,Integer>();
        Map<String,Integer> resultadoNao = new TreeMap<String,Integer>();
        List<Voto> votos = votoRepository.findAllVotosByPartido(partidoId);
        System.out.println(votos.size());

        for(Voto voto : votos){
            if(voto.getOpcao().equals("SIM")){
                int votacaoId = (int)voto.getVotacaoId();
                Votacao votacao= (Votacao)votacaoRepository.findById(votacaoId).get();
                Proposicao proposicao= proposicaoRepository.findById(votacao.getProposicaoId()).get();
                //System.out.println(proposicao);
                String[] palavras = proposicao.getIndexacao().split(",");
                if(palavras.length>3){
                    for(int i=0;i<3;i++){
                        if(resultadoSim.get(palavras[i])!=null){
                            int sum = resultadoSim.get(palavras[i]);
                            resultadoSim.put(palavras[i],sum+1);
                        }else{
                            resultadoSim.put(palavras[i],0);
                        }
                    }
                }

            }else if(voto.getOpcao().equals("NAO")){
                int votacaoId = voto.getVotacaoId();
                Votacao votacao= (Votacao)votacaoRepository.findById(votacaoId).get();
                Proposicao proposicao= proposicaoRepository.findById(votacao.getProposicaoId()).get();
                String[] palavras = proposicao.getIndexacao().split(",");
                if(palavras.length>3){
                    for(int i=0;i<3;i++){
                        if(resultadoNao.get(palavras[i])!=null){
                            int sum = resultadoNao.get(palavras[i]);
                            resultadoNao.put(palavras[i],sum+1);

                        }else{
                            resultadoNao.put(palavras[i],0);
                        }
                    }
                }

            }
        }
        return Pair.of(resultadoSim,resultadoNao);
    }
}
