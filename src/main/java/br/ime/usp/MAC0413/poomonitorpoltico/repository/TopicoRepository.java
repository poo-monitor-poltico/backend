package br.ime.usp.MAC0413.poomonitorpoltico.repository;

import br.ime.usp.MAC0413.poomonitorpoltico.model.Topico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicoRepository extends JpaRepository<Topico, Long> {

}
